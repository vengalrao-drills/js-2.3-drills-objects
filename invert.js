// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.

function invert(data, cb) {
  let newObject = {};
  for (let index in data) {  
    let addElement = cb(data, index);  // calling the callback function. with arguments data, index. receiveds data in the form of [values, keys]
    newObject[addElement[0]] = addElement[1]; // reassigning the newkeys in object.
  }
  return newObject; // returning the object
}

module.exports = invert; 
