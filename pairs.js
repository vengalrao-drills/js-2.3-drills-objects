// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(obj, cb) {
  let finalPairs = [];
  for (let index in obj) {
    finalPairs.push(cb(obj, index)); // calling a callback function object (obj), index  .
    // cb will return in the format of [key , value]
  }
  return finalPairs;
}

module.exports = pairs; // exporting the pairs to the toher modules
