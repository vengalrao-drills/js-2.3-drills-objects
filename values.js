function values(obj) {
  let values = Object.values(obj); // it is a methods to extract all the values
  return values;
}

module.exports = values;
