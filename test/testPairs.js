// Convert an object into a list of [key, value] pairs. 

const data = require("../data/testObject"); 
const pairs = require("../pairs");

function testPairs() {
  let finalPairs = pairs(data, (Array, index) => { // calling the pairs- it has arguments data and a callback function cb
    // return the array [key , value ]
    return [index, Array[index]];
  });
  return finalPairs; // Data received from pairs will be returned
}

console.log(testPairs());
