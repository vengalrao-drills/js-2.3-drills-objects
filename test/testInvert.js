// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

const data = require("../data/testObject");  
const invert = require("../invert");
function testInvert() {
  let invertFinal = invert(data, (object, index) => {  // calling the invert function with parameters as data, callbackfunction cb
    // this callback function returns the key  and  value  
    return [object[index], index]; // returns in the form of array
  });
  return invertFinal; 
}

console.log(testInvert());
