const data = require("../data/testObject");
const mapObject = require("../mapObject");

function testMapObject(data) {
  let finalData = mapObject(data, (a, key) => {
    // console.log(a, key)
    if (typeof a[key] == "string") { // if a[key] i string - then appending it to a value.
      return a[key] + "."; 
    } else {
      return a[key];
    }
  });
  return finalData;
}
 
console.log(testMapObject(data)); 
