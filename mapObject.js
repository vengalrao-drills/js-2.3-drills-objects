function mapObject(data, cb) {
  for (let i in data) {
    data[i] = cb(data, i);  // using a callback function. iterating through the function.
  }
  return data;
}
module.exports = mapObject;
